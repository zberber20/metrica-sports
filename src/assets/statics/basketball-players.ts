export const BEST_PLAYERS_EVER = [
  {
    id: 1,
    name: "Michael Jordan",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/893.png"
  },
  {
    id: 2,
    name: "Bill Russell",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/78049.png"
  },
  {
    id: 3,
    name: "Magic Johnson",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/77142.png"
  },
  {
    id: 4,
    name: "Kareem Abdul-Jabbar",
    photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaJ2T7ll3FsGpC8RE7mv4VHjGE8CQbxvyZLQ&usqp=CAU"
  },
  {
    id: 5,
    name: "Larry Bird",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/1449.png"
  },
  {
    id: 6,
    name: "LeBron James",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/2544.png"
  },
  {
    id: 7,
    name: "Wilt Chamberlain",
    photo: "https://upload.wikimedia.org/wikipedia/commons/7/72/Wilt_Chamberlain_headshot.jpg"
  },
  {
    id: 8,
    name: "Tim Duncan",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/1495.png"
  },
  {
    id: 9,
    name: "Shaquille O'Neal",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/406.png"
  },
  {
    id: 10,
    name: "Hakeem Olajuwon",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/165.png"
  },
  {
    id: 11,
    name: "Oscar Robertson",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/600015.png"
  },
  {
    id: 12,
    name: "Nikola Jokic",
    photo: "https://cdn.nba.com/headshots/nba/latest/1040x760/203999.png"
  },
]
