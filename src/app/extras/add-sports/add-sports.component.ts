import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-sports',
  templateUrl: './add-sports.component.html',
  styleUrls: ['./add-sports.component.scss']
})
export class AddSportsComponent implements OnInit {

  public items = [{ itemId: '', sportName: '', why: '' }];
  public item = {
    sportName: '',
    why: ''
  };

  constructor() { }

  ngOnInit(): void {
  }

  addItem = () => {
    this.items.push({
      itemId: '',
      sportName: '',
      why: ''
    });
  }
  deleteItem = (id: any) => {
    for (let i = 0; i < this.items.length; i++) {
      if (this.items.indexOf(this.items[i]) === id) {
        this.items.splice(i, 1);
        break;
      }
    }
  }
}
