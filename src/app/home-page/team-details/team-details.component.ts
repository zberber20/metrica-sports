import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';

export interface AllData{
  stats: Stats
}

export interface Stats{
  data: any[] | undefined
}

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  fakeTeamsDB: any[] = [
    {
      tid: 53,
      src: "https://upload.wikimedia.org/wikipedia/tr/a/a2/Leicester_City_logo.png?20100221122724",
      name: "Leicester City"
    },
    {
      tid: 338,
      src: "https://upload.wikimedia.org/wikipedia/tr/b/b6/Manchester_United_FC_logo.png?20110713132202",
      name: "Manchester United"
    }
  ]
  teamDetails: any;
  teamFundamentals: any;
  ngOnInit(): void {
    this.route.params.subscribe(i => {
      if (window.localStorage.getItem('matchScore')){
        const allData: AllData = JSON.parse(window.localStorage.getItem('matchScore')!)
        this.teamDetails = allData?.stats?.data?.filter((x: any) => x?.team_id === Number(i.id))[0]
        this.teamFundamentals = this.fakeTeamsDB.filter(y => y.tid === Number(i.id))[0]
        console.log("2222222", this.teamFundamentals, this.teamDetails)

        const shots = this.teamDetails?.shots
        delete shots['total']
        const config = new Chart("shots", {
          type: 'pie',
          data: {
            labels: Object.keys(shots),
            datasets: [{
              label: "# shots",
              data: Object.values(shots),
              backgroundColor: ['#6A2C70', '#B83B5E', '#F08A5D', '#F9ED69', '#00ADB5', '#FC5185', '#2B2E4A'],
            }]

          },
          options: {
            responsive: true,
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'Shots'
              }
            }
          },
        });

        const passes = this.teamDetails?.passes
        delete passes['total']
        const config2 = new Chart("passes", {
          type: 'pie',
          data: {
            labels: Object.keys(passes),
            datasets: [{
              label: "# passes",
              data: Object.values(passes),
              backgroundColor: ['#6A2C70', '#B83B5E', '#F08A5D', '#F9ED69', '#00ADB5', '#FC5185', '#2B2E4A'],
            }]

          },
          options: {
            responsive: true,
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'Passes'
              }
            }
          },
        });

        const general = {
          "fouls": this.teamDetails?.fouls,
          "corners": this.teamDetails?.corners,
          "offsides": this.teamDetails?.offsides,
          "saves": this.teamDetails?.saves,
          "substitutions": this.teamDetails?.substitutions,
          "goal_attempts": this.teamDetails?.goal_attempts,
          "free_kick": this.teamDetails?.free_kick,
          "throw_in": this.teamDetails?.throw_in,
          "ball_safe": this.teamDetails?.ball_safe,
          "goals": this.teamDetails?.goals,
          "penalties": this.teamDetails?.penalties,
          "injuries": this.teamDetails?.injuries,
          "tackles": this.teamDetails?.tackles
        }
        const config3 = new Chart("general", {
          type: 'bar',
          data: {
            labels: Object.keys(general),
            datasets: [{
              label: "# general informations",
              data: Object.values(general),
              backgroundColor: ['#6A2C70', '#B83B5E', '#F08A5D', '#F9ED69', '#00ADB5', '#FC5185', '#2B2E4A'],
            }]

          },
          options: {
            responsive: true,
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'General Informations'
              }
            }
          },
        });

      } else {
        this.router.navigate(['/'])
      }

    })

  }

}
