import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {

  constructor(
    public authService: AuthService
  ) {}

  registerForm: UntypedFormGroup = new UntypedFormGroup({});
  confirmPassword: string = "";
  ngOnInit(): void {
    this.registerForm = new UntypedFormGroup({
      'email': new UntypedFormControl('', [Validators.required, Validators.email]),
      'password': new UntypedFormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(10)])
    })
  }






}
