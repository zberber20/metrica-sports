import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinAllComponent } from './spin-all.component';


@NgModule({
  declarations: [SpinAllComponent],
  imports: [CommonModule],
  exports: [SpinAllComponent]
})
export class SharedModule { }
