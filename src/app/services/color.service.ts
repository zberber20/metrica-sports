import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  private colorRepo = new BehaviorSubject<string>('dark-wrapper');
  public color: Observable<string>

  private loggedInUser = new BehaviorSubject<boolean>(false);
  public loggedIn: Observable<boolean>

  constructor() {
    this.color = this.colorRepo.asObservable()
  }

  setColor = (val: string) => {
    this.colorRepo.next(val)
  }

  setLoggedInUser = (val: boolean) => {
    this.loggedInUser.next(val)
  }


}
