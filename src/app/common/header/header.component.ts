import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ColorService } from 'src/app/services/color.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private colorService: ColorService,
    public authService: AuthService
  ) {
    this.attendedColor$ = this.colorService.color
  }

  attendedColor$: any;
  collapsed: boolean = true;
  ngOnInit(): void {

  }

  toggleCollapsed = () => {
    this.collapsed = !this.collapsed;
  }
}
