import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HomePageComponent } from './home-page.component';
import { HomePageRoutingModule } from './home-page-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { NgChartjsModule } from 'ng-chartjs';


@NgModule({
  declarations: [HomePageComponent, TeamDetailsComponent],
  imports: [
    HomePageRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    SharedModule,
    NgChartjsModule
  ],
  bootstrap: [HomePageModule]
})
export class HomePageModule { }
