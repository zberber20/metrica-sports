import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ExtrasComponent } from './extras.component';
import { ExtrasRoutingModule } from './extras-routing.module';
import { DragulaModule } from 'ng2-dragula';
import { FeatherModule } from 'angular-feather';
import { DragComponent } from './drag/drag.component';
import { AddSportsComponent } from './add-sports/add-sports.component';


@NgModule({
  declarations: [ExtrasComponent, DragComponent, AddSportsComponent],
  imports: [
    ExtrasRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    DragulaModule.forRoot(),
    FeatherModule
  ]
})
export class ExtrasModule { }
