import { Component, OnInit } from '@angular/core';
import { ColorService } from '../services/color.service';
import { SportsApiService } from '../services/sports-api.service';

// export interface Statistics{
//   data: any | undefined
//   metrics: any| undefined
// }


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(
    private colorService: ColorService,
    private sportsApiService: SportsApiService
  ) { }

  spinner: boolean = false
  statistics: any;
  statsDetails = {
    home: {
      totalShots: 0,
      totalPasses: 0,
      totalAttacks: 0,
      totalFouls: 0,
      totalYellowcards: 0,
      totalRedcards: 0,
      goals: 0,
    },
    visitor: {
      totalShots: 0,
      totalPasses: 0,
      totalAttacks: 0,
      totalFouls: 0,
      totalYellowcards: 0,
      totalRedcards: 0,
      goals: 0,
    }
  }
  ngOnInit(): void {
  }

  setHeaderColor = (color: string) => {
    this.colorService.setColor(color)
  }

  getMatchStatistics = async () => {
    this.spinner = true
    await this.sportsApiService.getMatchStatistic().subscribe(res => {
      this.statistics = res['data']
      const stats = res['data']
      this.statsDetails['home']['totalShots'] = Math.floor((stats?.stats?.data[0]?.shots?.total / (stats?.stats?.data[1]?.shots?.total + stats?.stats?.data[0]?.shots?.total))*100)
      this.statsDetails['home']['totalPasses'] = Math.floor((stats?.stats?.data[0]?.passes?.total / (stats?.stats?.data[1]?.passes?.total + stats?.stats?.data[0]?.passes?.total))*100)
      this.statsDetails['home']['totalAttacks'] = Math.floor((stats?.stats?.data[0]?.attacks?.attacks / (stats?.stats?.data[1]?.attacks?.attacks + stats?.stats?.data[0]?.attacks?.attacks))*100)
      this.statsDetails['home']['totalFouls'] = Math.floor((stats?.stats?.data[0]?.fouls / (stats?.stats?.data[1]?.fouls + stats?.stats?.data[0]?.fouls))*100)
      this.statsDetails['home']['goals'] = Math.floor((stats?.stats?.data[0]?.goals / (stats?.stats?.data[1]?.goals + stats?.stats?.data[0]?.goals))*100)

      this.statsDetails['visitor']['totalShots'] = Math.floor((stats?.stats?.data[1]?.shots?.total / (stats?.stats?.data[1]?.shots?.total + stats?.stats?.data[0]?.shots?.total))*100)
      this.statsDetails['visitor']['totalPasses'] = Math.floor((stats?.stats?.data[1]?.passes?.total / (stats?.stats?.data[1]?.passes?.total + stats?.stats?.data[0]?.passes?.total))*100)
      this.statsDetails['visitor']['totalAttacks'] = Math.floor((stats?.stats?.data[1]?.attacks?.attacks / (stats?.stats?.data[1]?.attacks?.attacks + stats?.stats?.data[0]?.attacks?.attacks))*100)
      this.statsDetails['visitor']['totalFouls'] = Math.floor((stats?.stats?.data[1]?.fouls / (stats?.stats?.data[1]?.fouls + stats?.stats?.data[0]?.fouls))*100)
      this.statsDetails['visitor']['goals'] = Math.floor((stats?.stats?.data[1]?.goals / (stats?.stats?.data[1]?.goals + stats?.stats?.data[0]?.goals))*100)

      this.spinner = false

      window.localStorage.setItem('matchScore', JSON.stringify(res['data']))

    }, err => this.spinner = false)
  }

  getCountries = async () => {
    this.sportsApiService.getCountries().subscribe(res => {
      console.log(res)
    })
  }

}
