import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BEST_PLAYERS_EVER } from 'src/assets/statics/basketball-players';

@Component({
  selector: 'app-drag',
  templateUrl: './drag.component.html',
  styleUrls: ['./drag.component.scss']
})
export class DragComponent implements OnInit {
  @ViewChild('bestPlayers') el:ElementRef;

  constructor() { }

  players: any[];
  finalPlayers: any[] = []
  dreamTeam: any[] = []
  ngOnInit(): void {
    this.players = BEST_PLAYERS_EVER
  }

  consolePlayers(){
    this.finalPlayers = []
    const arr: any[] = this.el.nativeElement.querySelectorAll('li')

    for(let i of arr) {
      this.finalPlayers.push(i.id)
    }

    if(this.finalPlayers.length !== 5){
      alert('please choose 5 players')
      this.finalPlayers=[]
      return
    }

    this.dreamTeam = this.finalPlayers.map((id) => this.players.find((el) => el.id === Number(id)));

  }

}
