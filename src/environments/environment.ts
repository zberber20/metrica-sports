// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Import the functions you need from the SDKs you need
  // import { initializeApp } from "firebase/app";
  // import { getAnalytics } from "firebase/analytics";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig: {
    apiKey: "AIzaSyAj0LxeiXPf48Hsdt72hf4dXL-ap0ZH9wQ",
    authDomain: "metrica-sports.firebaseapp.com",
    projectId: "metrica-sports",
    storageBucket: "metrica-sports.appspot.com",
    messagingSenderId: "761173134507",
    // appId: "1:761173134507:web:8b42acf25a1f8d0f5efbee",
    // measurementId: "G-BNCCBSW7R4"
  },

  // Initialize Firebase
  // const app = initializeApp(firebaseConfig);
  // const analytics = getAnalytics(app);
  SPORTSAPI: {
    "X-Auth-Token": "e16ee584f3874f2d83c42ba8be6d4f77",
    "url": "http://api.football-data.org/v4/matches/327117",
  },

  SPORTSDATAAPI: {
    api: "6311cae0-83be-11ed-a62f-8b8ed02704fc",
    url: "https://app.sportdataapi.com/api/v1/"
  },

  SPORTMONKSAPI: {
    token: "PoGPG7f1i2EAsXdtWeHuPQJbEdYh0oee3MvKD0RwzhmLP9oK7kBFJeii25Tg",
    url: "https://soccer.sportmonks.com/api/v2.0"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
