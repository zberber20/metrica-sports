import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SportsApiService {

  constructor(private httpclient: HttpClient) { }

  getLastMatchStatistics = () => {
    // return this.httpclient.get(environment.SPORTSDATAAPI.url+'soccer/matches?apikey='+environment.SPORTSDATAAPI.api+'&season_id=496&date_from=2022-01-01&date_to=2022-06-01')
    return this.httpclient.get('https://app.sportdataapi.com/api/v1/soccer/matches?apikey=6311cae0-83be-11ed-a62f-8b8ed02704fc&season_id=496&date_from=2020-09-19')
  }

  getCountries = () => {
    return this.httpclient.get(environment.SPORTMONKSAPI.url+'/countries'+'?api_token='+environment.SPORTMONKSAPI.token)
  }

  getMatchStatistic = () => {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    }
    return this.httpclient.get(environment.SPORTMONKSAPI.url+'/fixtures/16475287'+'?api_token='+environment.SPORTMONKSAPI.token+'&include=stats', options)
  }
}
