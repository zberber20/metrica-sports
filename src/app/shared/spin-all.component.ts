import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spin-all',
  template: `<div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>`,
})

export class SpinAllComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


}
