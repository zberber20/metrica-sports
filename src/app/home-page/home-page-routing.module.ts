import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page.component';
import { TeamDetailsComponent } from './team-details/team-details.component';

const routes: Routes = [
	{
		path : '',
		component : HomePageComponent
	},
	{
		path : ':id',
		component : TeamDetailsComponent
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule { }
