import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Event, NavigationStart, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from './services/auth.service';
import { ColorService } from './services/color.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit{
  title = 'metrica-sports';
  url: string = ''
  show: boolean = true
  loggedInUser$: any;

  constructor(
    public router: Router,
    location: Location,
    private authService: AuthService,
    private colorService: ColorService
  ) {
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        if (
          event.url == '/login' ||
          event.url == '/register' ||
          event.url == '/verify-email'
        ) {
          this.show = false
        } else {
          this.show = true
        }
      }
    });

  }

  ngOnInit(): void {
  }

}
